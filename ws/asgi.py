"""
ASGI config for ws project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/asgi/
"""

import os

from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from django.core.asgi import get_asgi_application

# from ws.websocket import websocket_application
from realtime import consumers, routing

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ws.settings")

application = ProtocolTypeRouter(
    {
        "http": get_asgi_application(),
        "websocket": AuthMiddlewareStack(URLRouter(routing.websocket_urlpatterns))
    }
)

# async def application(scope, receive, send):
#     if scope['type'] == 'http':
#         # Let Django handle HTTP requests
#         await dj_application(scope, receive, send)
#     elif scope['type'] == 'websocket':
#         # We'll handle Websocket connections here
#         await websocket_application(scope, receive, send)
#         pass
#     else:
#         raise NotImplementedError(f"Unknown scope type {scope['type']}")
