"""ws URL Configuration
"""
from django.conf.urls import include
from django.contrib import admin
from django.urls import path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('realtime/', include('realtime.urls')),
    path('doc/', include('doc.urls')),
    path('', include('realtime.urls')),

]
