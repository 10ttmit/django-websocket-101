# chat/urls.py
from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('board/<str:channel>/', views.board, name='board'),
    path('push/<str:channel>/', views.push, name='push'),
]