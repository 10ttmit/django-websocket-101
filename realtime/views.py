from django.shortcuts import render
from channels.layers import get_channel_layer


async def index(request):
    chan_layers = get_channel_layer()
    for g in chan_layers.groups:
        await chan_layers.group_send(
            g,
            {"type": "notify", "message": "entered Lobby"},
        )
    return render(request, "chan_index.html")


def board(request, channel):
    return render(request, "chan_board.html", {"channel": channel})


async def push(request, channel):
    chan_layers = get_channel_layer()
    chanID = f"chan_{channel}"
    print("chan groups", chan_layers.groups, chanID, chanID in chan_layers.groups)
    if chanID in chan_layers.groups:
        await chan_layers.group_send(
            chanID,
            {"type": "notify", "message": f"entered Channel {channel}..."},
        )
    return render(request, "chan_push.html", {"channel": channel})
