import json
from channels.generic.websocket import AsyncWebsocketConsumer

class ChannelConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.channel = self.scope['url_route']['kwargs']['channel']
        self.chanID = 'chan_%s' % self.channel

        # Join group
        await self.channel_layer.group_add(
            self.chanID,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        # Leave group
        await self.channel_layer.group_discard(
            self.chanID,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        # Send message to room group
        await self.channel_layer.group_send(
            self.chanID,
            {
                'type': 'notify',
                'message': message
            }
        )

    # Receive message from room group
    async def notify(self, event):
        message = event['message']

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message
        }))
