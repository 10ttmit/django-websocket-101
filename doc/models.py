from django.db import models

# Create your models here.
class TrackingReport(models.Model):
    route = models.CharField(max_length=200)
    arrival_time = models.TimeField(null=True, blank=True)
    shift_seq = models.IntegerField(default=1)
    run_seq = models.IntegerField(default=1)
    truck_license = models.CharField(max_length=200)
    eta = models.DateTimeField(null=True, blank=True)
    confirm = models.BooleanField(default=False)


# from random import choices, randint, shuffle
# import string
# from datetime import datetime


# def rand_string(length=8):
#     return "".join(choices(string.ascii_letters, k=length))


# def rand_int(length=8):
#     return "".join(choices(string.digits, k=length))


# def random():
#     today = datetime.now()
#     return {
#         "route": rand_string(),
#         "shift_seq": rand_int(1),
#         "run_seq": rand_int(1),
#         "truck_license": rand_string(),
#         "confirm": choices([False, True])[0],
#         "arrival_time": f"{randint(0,23):02d}:{randint(0,59):02d}",
#         "eta": f'{today.strftime("%Y-%m-%d")}T{randint(0,23):02d}:{randint(0,59):02d}Z',
#     }


# for i in range(300):
#     TrackingReport.objects.create(**random())