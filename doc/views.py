from django.shortcuts import render
from django.template.loader import render_to_string

from django.http import HttpResponse
from pdfkit import PDFKit

from doc.models import TrackingReport


# Create your views here.
def ReportPDFView(request):
    qs = TrackingReport.objects.all()
    # print(data)

    tab = render_to_string("tab.html", {"list": qs})

    options = {
        'page-size': 'A4',
        'margin-top': '0.75in',
        'margin-right': '0.75in',
        'margin-bottom': '0.75in',
        'margin-left': '0.75in',
        "footer-right": "[page] of [topage]",
        'encoding': "UTF-8",
        'custom-header': [
            ('Accept-Encoding', 'gzip')
        ],
        'no-outline': None
    }

    r = PDFKit(tab, "string", options)
    pdf = r.to_pdf()
    response = HttpResponse(content_type="application/pdf")
    response.write(pdf)
    return response
