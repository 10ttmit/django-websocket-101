# Django w/ websocket

This is a basic implementation of websocket with *channels* which will utilize some sort of backend memory: either memory or redis.

## dependecies

* django 3+
* daphne or similar

## How to test

    ./manage.py runserver

or

    daphne ws.asgi:application

Then go to url `http://localhost:8000/realtime/board/abc` on one page and `http://localhost:8000/realtime` on another page to see "real-time" push information. Go to "abc" room to send any messages.
